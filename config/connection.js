const mongoose = require("mongoose");
require("dotenv").config();
const log = require("lambda-log");

let cachedDb = null;

async function connectDB() {
  if (cachedDb && mongoose.connection.readyState === 1) {
    log.info("Using existing database connection");
    return cachedDb;
  }
  try {
    await mongoose.connect(process.env.DB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    log.info("Connected to MongoDB");
    cachedDb = mongoose.connection;
    return cachedDb;
  } catch (error) {
    log.error("Error connecting to MongoDB:", error);
    throw error;
  }
}


module.exports = connectDB;
