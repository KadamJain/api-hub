const mongoose = require("mongoose");

const planetSchema = new mongoose.Schema({
  id: {
    type: String,
    unique: true,
    required: true,
  },
  name: {
    type: String,
    unique: true,
    required: true,
  },
  type: String,
  position_from_sun: Number,
  number_of_moons: Number,
  hasRing: Boolean,
  distance_from_sun_in_km: String,
  diameter_in_km: Number,
  mass_earth_masses: Number,
  orbital_period_in_days: Number,
  rotation_period_in_hours: Number,
  atmosphere: String,
  surface_temperature_in_celsius: Number,
  gravity_earth_gravities: Number,
  discovery_status: String,
  description: String,
});

const Planet = mongoose.model("Planet", planetSchema);

module.exports = Planet;
