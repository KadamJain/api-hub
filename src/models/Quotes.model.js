const mongoose = require("mongoose");

const QuoteSchema = new mongoose.Schema({
  id: {
    type: String,
    unique: true,
    required: true,
  },
  quote: {
    type: String,
    unique: true,
    required: true,
  },
  quotedBy: {
    type: String,
    required: true,
  },
});

const Quote = mongoose.model("Quote", QuoteSchema);

module.exports = Quote;
