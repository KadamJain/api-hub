const mongoose = require("mongoose");

const IdiomSchema = new mongoose.Schema({
  id: {
    type: String,
    unique: true,
    required: true,
  },
  idiom: {
    type: String,
    unique: true,
    required: true,
  },
  explanation: {
    type: String,
    required: true,
  },
});

const Idiom = mongoose.model("Idiom", IdiomSchema);

module.exports = Idiom;
