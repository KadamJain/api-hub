const mongoose = require("mongoose");

const FactSchema = new mongoose.Schema({
  id: {
    type: String,
    unique: true,
    required: true,
  },
  fact: {
    type: String,
    unique: true,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
});

const Fact = mongoose.model("Fact", FactSchema);

module.exports = Fact;
