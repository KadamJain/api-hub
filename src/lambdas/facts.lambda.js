const {
  listAllFactsService,
  getFactByIdService,
  listFactsByCategoriesService,
} = require("../services/facts.services");
const log = require("lambda-log");
const connectDB = require("../../config/connection");

async function listAllFacts() {
  try {
    await connectDB();
    const response = await listAllFactsService();
    return response;
  } catch (err) {
    log.error(err);
  }
}

async function getFactById(id) {
  try {
    await connectDB();
    const response = await getFactByIdService(id);
    return response;
  } catch (err) {
    log.error(err);
  }
}
async function listFactsByCategories(categories) {
  try {
    await connectDB();
    const response = await listFactsByCategoriesService(categories);
    return response;
  } catch (err) {
    log.error(err);
  }
}

module.exports = {
  listAllFacts,
  getFactById,
  listFactsByCategories,
};
