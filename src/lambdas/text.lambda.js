const {
  generateRandomPasswordService,
  generateLoremDummyTextService,
} = require("../services/text.services");
const log = require("lambda-log");

async function generateRandomPassword(length) {
  try {
    const response = await generateRandomPasswordService(length);
    return response;
  } catch (err) {
    log.error(err);
  }
}
async function generateLoremDummyText({
  numOfParagraphs,
  numOfSentencesPerPara,
}) {
  try {
    const response = await generateLoremDummyTextService({
      numParagraphs: numOfParagraphs,
      numSentencesPerParagraph: numOfSentencesPerPara,
    });
    return response;
  } catch (err) {
    log.error(err);
  }
}

module.exports = {
  generateRandomPassword,
  generateLoremDummyText,
};
