const {
  generateQRService,
  generatePaymentQRService,
} = require("../services/qr.services");
const log = require("lambda-log");

async function generateQR(text) {
  try {
    const response = await generateQRService(text);
    return response;
  } catch (err) {
    log.error(err);
  }
}
async function generatePaymentQR(body) {
  try {
    const response = await generatePaymentQRService(body);
    return response;
  } catch (err) {
    log.error(err);
  }
}

module.exports = {
  generateQR,
  generatePaymentQR,
};
