const {
  listAllIdiomsService,
  getIdiomByIdService,
} = require("../services/idioms.services");
const log = require("lambda-log");
const connectDB = require("../../config/connection");

async function listAllIdioms() {
  try {
    await connectDB();
    const response = await listAllIdiomsService();
    return response;
  } catch (err) {
    log.error(err);
  }
}

async function getIdiomById(id) {
  try {
    await connectDB();
    const response = await getIdiomByIdService(id);
    return response;
  } catch (err) {
    log.error(err);
  }
}

module.exports = {
  listAllIdioms,
  getIdiomById,
};
