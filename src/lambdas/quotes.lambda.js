const {
  listAllQuotesService,
  getQuoteByIdService,
} = require("../services/quotes.services");
const connectDB = require("../../config/connection");
const log = require("lambda-log");

async function listAllQuotes() {
  try {
    await connectDB();
    const response = await listAllQuotesService();
    return response;
  } catch (err) {
    log.error(err);
  }
}

async function getQuoteById(id) {
  try {
    await connectDB();
    const response = await getQuoteByIdService(id);
    return response;
  } catch (err) {
    log.error(err);
  }
}

module.exports = {
  listAllQuotes,
  getQuoteById,
};
