const { log } = require("lambda-log");
const {
  listAllPlanetsService,
  getPlanetByIdService,
} = require("../services/planets.services");
const connectDB = require("../../config/connection");

async function listAllPlanets() {
  try {
    await connectDB();
    const response = await listAllPlanetsService();
    return response;
  } catch (err) {
    log.error(err);
  }
}
async function getPlanetById(id) {
  try {
    await connectDB();
    const response = await getPlanetByIdService(id);
    return response;
  } catch (err) {
    log.error(err);
  }
}

module.exports = {
  listAllPlanets,
  getPlanetById,
};
