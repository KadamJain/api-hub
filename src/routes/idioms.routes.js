const router = require("express").Router();
const idiomsController = require("../lambdas/idioms.lambda");
const { checkParamsUUIDSchema } = require("../validation-schema/common.schema");
const { validator } = require("../middlewares/validator");

router.get("/", idiomsController.listAllIdioms);
router.get(
  "/:id",
  checkParamsUUIDSchema,
  validator,
  idiomsController.getIdiomById
);

module.exports = router;
