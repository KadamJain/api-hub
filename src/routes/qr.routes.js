const router = require("express").Router();
const QRController = require("../lambdas/qr.lambda");
const { validator } = require("../middlewares/validator");
const {
  generateQRSchema,
  generatePaymentQRSchema,
} = require("../validation-schema/qr.schema");

router.get("/", generateQRSchema, validator, QRController.generateQR);
router.post(
  "/payment",
  generatePaymentQRSchema,
  validator,
  QRController.generatePaymentQR
);

module.exports = router;
