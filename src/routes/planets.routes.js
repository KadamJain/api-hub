const router = require("express").Router();
const planetController = require("../lambdas/planets.lambda");
const { checkParamsUUIDSchema } = require("../validation-schema/common.schema");
const { validator } = require("../middlewares/validator");

router.get("/", planetController.listAllPlanets);
router.get(
  "/:id",
  checkParamsUUIDSchema,
  validator,
  planetController.getPlanetById
);

module.exports = router;
