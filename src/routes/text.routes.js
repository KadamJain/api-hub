const router = require("express").Router();
const textController = require("../lambdas/text.lambda");
const { validator } = require("../middlewares/validator");
const { generatePasswordSchema, generateLoremTextSchema } = require("../validation-schema/text.schema");

router.get(
  "/generate/password",
  generatePasswordSchema,
  validator,
  textController.generateRandomPassword
);

router.get(
  "/generate/lorem",
  generateLoremTextSchema,
  validator,
  textController.generateLoremDummyText
);

module.exports = router;
