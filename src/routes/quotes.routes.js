const router = require("express").Router();
const quotesController = require("../lambdas/quotes.lambda");
const { checkParamsUUIDSchema } = require("../validation-schema/common.schema");
const { validator } = require("../middlewares/validator");

router.get("/", quotesController.listAllQuotes);
router.get(
  "/:id",
  checkParamsUUIDSchema,
  validator,
  quotesController.getQuoteById
);

module.exports = router;
