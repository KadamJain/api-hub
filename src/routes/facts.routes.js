const router = require("express").Router();
const factsController = require("../lambdas/facts.lambda");
const { checkParamsUUIDSchema } = require("../validation-schema/common.schema");
const { validator } = require("../middlewares/validator");

router.get("/", factsController.listAllFacts);
router.get(
  "/:id",
  checkParamsUUIDSchema,
  validator,
  factsController.getFactById
);
router.post("/", factsController.listFactsByCategories);

module.exports = router;
