const TEXT_CONSTANTS = {
  MIN_PASSWORD_LENGTH: 8,
  UPPERCASE_CHARS: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
  LOWERCASE_CHARS: "abcdefghijklmnopqrstuvwxyz",
  SPECIAL_CHARS: "!@#$%^&*",
  NUMERIC_CHARS: "0123456789",
  WORDS: [
    "lorem",
    "ipsum",
    "dolor",
    "sit",
    "amet",
    "consectetur",
    "adipiscing",
    "elit",
    "sed",
    "do",
    "eiusmod",
    "tempor",
  ],
};

module.exports = {
  TEXT_CONSTANTS,
};
