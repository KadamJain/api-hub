const {
  listAllFacts,
  getFactById,
  listFactsByCategories,
} = require("../lambdas/facts.lambda");
const log = require("lambda-log");
const { _400 } = require("../utils/responses");

module.exports.handler = async (event) => {
  try {
    log.info("event captured for Facts", event);
    const { pathParameters, httpMethod, body, resource } = event;
    let response;
    switch (resource) {
      case "/facts": {
        if (httpMethod === "GET") {
          response = await listAllFacts();
        } else if (httpMethod === "POST") {
          if (!body) response = _400({ message: "Missing parameters!" });
          else {
            const { categories } = JSON.parse(body);
            log.info("categories", categories);
            response = await listFactsByCategories(categories);
          }
        }
        break;
      }
      case "/facts/{id}": {
        const { id } = pathParameters;
        response = await getFactById(id);
        break;
      }
      default:
        response = {
          statusCode: 404,
          body: "Endpoint not found",
        };
        break;
    }
    return response;
  } catch (err) {
    log.error("Error at handler", err);
  }
};
