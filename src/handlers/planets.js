const { listAllPlanets, getPlanetById } = require("../lambdas/planets.lambda");
const log = require("lambda-log");

module.exports.handler = async (event) => {
  try {
    log.info("event captured for Planets", event);
    const { pathParameters, resource } = event;
    let response;
    switch (resource) {
      case "/planets": {
        response = await listAllPlanets();
        break;
      }
      case "/planets/{id}": {
        const { id } = pathParameters;
        response = await getPlanetById(id);
        break;
      }
      default:
        response = {
          statusCode: 404,
          body: "Endpoint not found",
        };
        break;
    }
    return response;
  } catch (err) {
    log.error("Error at handler", err);
  }
};
