const { listAllIdioms, getIdiomById } = require("../lambdas/idioms.lambda");
const log = require("lambda-log");

module.exports.handler = async (event) => {
  try {
    log.info("event captured for Facts", event);
    const { pathParameters, resource } = event;
    let response;
    switch (resource) {
      case "/idioms": {
        response = await listAllIdioms();
        break;
      }
      case "/idioms/{id}": {
        const { id } = pathParameters;
        response = await getIdiomById(id);
        break;
      }
      default:
        response = {
          statusCode: 404,
          body: "Endpoint not found",
        };
        break;
    }
    return response;
  } catch (err) {
    log.error("Error at handler", err);
  }
};
