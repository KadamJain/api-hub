const { generateQR, generatePaymentQR } = require("../lambdas/qr.lambda");
const log = require("lambda-log");
const { _400 } = require("../utils/responses");

module.exports.handler = async (event) => {
  try {
    log.info("event captured for QR", event);
    const { queryStringParameters, resource, body } = event;
    let response;
    switch (resource) {
      case "/qr": {
        const { text } = queryStringParameters;
        response = await generateQR(text);
        break;
      }
      case "/qr/payment": {
        if (!body) {
          response = _400({ message: "Missing parameters!" });
        } else {
          const params = JSON.parse(body);
          response = await generatePaymentQR(params);
        }
        break;
      }
      default:
        response = {
          statusCode: 404,
          body: "Endpoint not found",
        };
        break;
    }
    return response;
  } catch (err) {
    log.error("Error at handler", err);
  }
};
