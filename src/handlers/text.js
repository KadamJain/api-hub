const {
  generateRandomPassword,
  generateLoremDummyText,
} = require("../lambdas/text.lambda");
const log = require("lambda-log");
const { _400 } = require("../utils/responses");

module.exports.handler = async (event) => {
  try {
    log.info("event captured for Text", event);
    const { queryStringParameters, resource } = event;
    let response;
    switch (resource) {
      case "/generate/password": {
        const { length } = queryStringParameters;
        if (length > 50 || length < 4)
          return _400({ message: "Password length must be between 4 to 50" });
        response = await generateRandomPassword(length);
        break;
      }
      case "/generate/lorem":
        {
          const { numOfParagraphs, numOfSentencesPerPara } =
            queryStringParameters;
          response = await generateLoremDummyText({
            numOfParagraphs,
            numOfSentencesPerPara,
          });
        }
        break;
      default:
        response = {
          statusCode: 404,
          body: "Endpoint not found",
        };
        break;
    }
    return response;
  } catch (err) {
    log.error("Error at handler", err);
  }
};
