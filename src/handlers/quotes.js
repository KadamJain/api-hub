const { listAllQuotes, getQuoteById } = require("../lambdas/quotes.lambda");
const log = require("lambda-log");

module.exports.handler = async (event) => {
  try {
    log.info("event captured for Quotes", event);
    const { pathParameters, resource } = event;
    let response;
    switch (resource) {
      case "/quotes": {
        response = await listAllQuotes();
        break;
      }
      case "/quotes/{id}":
        {
          const { id } = pathParameters;
          response = await getQuoteById(id);
        }
        break;
      default:
        response = {
          statusCode: 404,
          body: "Endpoint not found",
        };
        break;
    }
    return response;
  } catch (err) {
    log.error("Error at handler", err);
  }
};
