const { param } = require("express-validator");

const checkParamsUUIDSchema = [
  param("id").isUUID().withMessage("Invalid search id!"),
];

module.exports = { checkParamsUUIDSchema };
