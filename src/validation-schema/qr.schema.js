const { check, body } = require("express-validator");

const generateQRSchema = [
  check("text").notEmpty().withMessage("text required!"),
];

const generatePaymentQRSchema = [
  body("payeeVPA")
    .notEmpty()
    .isLength({ min: 5 })
    .isString()
    .withMessage("valid reciever VPA required!"),
  body("payeeName")
    .notEmpty()
    .isLength({ min: 4 })
    .isString()
    .withMessage("valid reciever name required!"),
  body("amount").optional().isInt({min: 1}).withMessage("Invalid Amount!"),
  body("transactionId")
    .optional()
    .isString()
    .withMessage("Invalid transactionId!"),
  body("transactionRef")
    .optional()
    .isString()
    .isLength({ max: 35 })
    .withMessage("Invalid transactionRef!"),
  body("transactionNote")
    .optional()
    .isString()
    .withMessage("Invalid transactionNote"),
  body("payeeMerchantCode")
    .optional()
    .isString()
    .withMessage("Invalid payeeMerchantCode!"),
  body("minimumAmount")
    .optional()
    .isInt({min: 1})
    .isString()
    .withMessage("Invalid minimumAmount"),
  body("currency").optional().isString().withMessage("Invalid currency"),
];

module.exports = {
  generateQRSchema,
  generatePaymentQRSchema,
};
