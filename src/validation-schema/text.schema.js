const { query } = require("express-validator");

const generatePasswordSchema = [
  query("length")
    .isInt({ min: 4, max: 50 })
    .withMessage("Password length must be in range between 4 to 50"),
];

const generateLoremTextSchema = [
  query("numOfParagraphs")
    .isInt({ min: 1, max: 100 })
    .withMessage("Number of paragraphs must be in range between 1 to 100"),
  query("numOfSentencesPerPara")
    .isInt({ min: 1, max: 20 })
    .withMessage("Number of sentences per paragraph must be in range between 1 to 20"),
];

module.exports = { generatePasswordSchema, generateLoremTextSchema };
