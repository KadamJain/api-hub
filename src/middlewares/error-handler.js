function errorHandler(err, req, res, next) {
  console.error(err.stack);
  res.status(500).json({ error: "Something went wrong!" });
  next(err);
}

module.exports = errorHandler;

module.exports = {
  errorHandler,
};
