require("dotenv").config();
const { TEXT_CONSTANTS } = require("../constants/text-constants");
const { generateText } = require("../utils/lorem-ipsum");
const { _200 } = require("../utils/responses");

async function generateRandomPasswordService(
  length = TEXT_CONSTANTS.MIN_PASSWORD_LENGTH
) {
  const uppercaseChars = TEXT_CONSTANTS.UPPERCASE_CHARS;
  const lowercaseChars = TEXT_CONSTANTS.LOWERCASE_CHARS;
  const specialChars = TEXT_CONSTANTS.SPECIAL_CHARS;
  const numericChars = TEXT_CONSTANTS.NUMERIC_CHARS;
  const allChars =
    uppercaseChars + lowercaseChars + specialChars + numericChars;
  const requiredChars = [
    uppercaseChars[Math.floor(Math.random() * uppercaseChars.length)],
    specialChars[Math.floor(Math.random() * specialChars.length)],
    numericChars[Math.floor(Math.random() * numericChars.length)],
  ];
  let password = requiredChars.join("");
  for (let i = password.length; i < length; i++) {
    const randomChar = allChars[Math.floor(Math.random() * allChars.length)];
    password += randomChar;
  }

  password = password.split("");
  for (let i = password.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [password[i], password[j]] = [password[j], password[i]];
  }
  return _200({
    message: "password generated successfully!",
    data: {
      password: password.join(""),
    },
  });
}

async function generateLoremDummyTextService({
  numParagraphs,
  numSentencesPerParagraph,
}) {
  const loremText = generateText(
    numParagraphs,
    numSentencesPerParagraph,
    5,
    15
  );
  return _200({
    message: "lorem text generated successfully!",
    data: {
      text: loremText,
    },
  });
}
module.exports = {
  generateRandomPasswordService,
  generateLoremDummyTextService,
};
