const Fact = require("../models/Facts.model");
const { _200, _400 } = require("../utils/responses");

async function listAllFactsService(query = {}) {
  const facts = await Fact.find(query, { _id: 0, __v: 0 });
  return _200({
    message: "facts fetched successfully!",
    data: {
      totalCount: facts.length,
      facts,
    },
  });
}

async function getFactByIdService(id) {
  if (!id) {
    return _400({ message: "Invalid Id" });
  }
  const fact = await Fact.findOne({ id }, { _id: 0, __v: 0 });
  return _200({
    message: "fact fetched successfully!",
    data: {
      fact,
    },
  });
}

async function listFactsByCategoriesService(categories = []) {
  if (!categories || !categories.length) {
    return _400({ message: "Atleast 1 or more categories required" });
  }
  const facts = await Fact.find(
    { category: { $in: categories } },
    { _id: 0, __v: 0 }
  );
  return _200({
    message: "facts fetched successfully!",
    data: {
      totalCount: facts.length,
      facts,
    },
  });
}
module.exports = {
  listAllFactsService,
  getFactByIdService,
  listFactsByCategoriesService,
};
