const Idiom = require("../models/Idioms.model");
const { _200 } = require("../utils/responses");

async function listAllIdiomsService(query = {}) {
  const idioms = await Idiom.find(query, { _id: 0, __v: 0 });
  return _200({
    message: "idioms fetched successfully!",
    data: {
      totalCount: idioms.length,
      idioms,
    },
  });
}

async function getIdiomByIdService(id) {
  const idiom = await Idiom.findOne({ id }, { _id: 0, __v: 0 });
  return _200({
    message: "Idiom fetched successfully!",
    data: {
      idiom,
    },
  });
}

module.exports = {
  listAllIdiomsService,
  getIdiomByIdService,
};
