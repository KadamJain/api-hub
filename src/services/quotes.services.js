const Quote = require("../models/Quotes.model");
const { _200 } = require("../utils/responses");

async function listAllQuotesService(query = {}) {
  const quotes = await Quote.find(query, { _id: 0, __v: 0 });
  return _200({
    message: "quotes fetched successfully!",
    data: {
      totalCount: quotes.length,
      quotes,
    },
  });
}

async function getQuoteByIdService(id) {
  const quote = await Quote.findOne({ id }, { _id: 0, __v: 0 });
  return _200({
    message: "quote fetched successfully!",
    data: {
      quote,
    },
  });
}

module.exports = {
  listAllQuotesService,
  getQuoteByIdService,
};
