const Planet = require("../models/Planets.model");
const { _200, _400 } = require("../utils/responses");

async function listAllPlanetsService() {
  const planets = await Planet.find({}, { _id: 0, __v: 0 });
  return _200({
    message: "planets fetched successfully!",
    data: {
      totalCount: planets.length,
      planets,
    },
  });
}
async function getPlanetByIdService(id) {
  if (!id) {
    return _400({ message: "Invalid Id" });
  }
  const planet = await Planet.findOne({ id }, { _id: 0, __v: 0 });
  return _200({
    message: "planet fetched successfully!",
    data: {
      planet,
    },
  });
}
module.exports = {
  listAllPlanetsService,
  getPlanetByIdService,
};
