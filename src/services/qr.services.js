const QRCode = require("qrcode");
const { _200, _400 } = require("../utils/responses");
const { buildUrl } = require("../utils/qr");

async function generateQRService(text = "") {
  if (!text) {
    return _400({ message: "Invalid Id" });
  }
  const qr = await QRCode.toDataURL(text);
  return _200({
    message: "QR code generated!",
    data: {
      QR: qr,
    },
  });
}

async function generatePaymentQRService({
  payeeVPA: pa,
  payeeName: pn,
  payeeMerchantCode: me,
  transactionId: tid,
  transactionRef: tr,
  transactionNote: tn,
  amount: am,
  minimumAmount: mam,
  currency: cu,
}) {
  if (!pa || !pn) {
    return _400({ message: "Invalid payeeVPA or payeeName" });
  }
  const data = await new Promise((resolve, reject) => {
    let intent = "upi://pay?";
    if (pa) intent = buildUrl.call(intent, { pa, pn });
    if (am) intent = buildUrl.call(intent, { am });
    if (mam) intent = buildUrl.call(intent, { mam });
    if (cu) intent = buildUrl.call(intent, { cu });
    if (me) intent = buildUrl.call(intent, { me });
    if (tid) intent = buildUrl.call(intent, { tid });
    if (tr) intent = buildUrl.call(intent, { tr }); // tr: transactionRef up to 35 digits
    if (tn) intent = buildUrl.call(intent, { tn });
    intent = intent.substring(0, intent.length - 1);

    QRCode.toDataURL(intent)
      .then((base64Data) => resolve({ qr: base64Data, intent }))
      .catch((err) =>
        reject(new Error("Unable to generate UPI QR Code.\n" + err))
      );
  });
  return _200({
    message: "QR code generated!",
    data: {
      payeeVPA: pa,
      payeeName: pn,
      QR: data.qr ?? null,
    },
  });
}

module.exports = {
  generateQRService,
  generatePaymentQRService,
};
