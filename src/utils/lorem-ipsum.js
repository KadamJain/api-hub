const { TEXT_CONSTANTS } = require("../constants/text-constants");
const WORDS = TEXT_CONSTANTS.WORDS;

function generateRandomInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function generateRandomWords(words, min, max) {
  const length = generateRandomInteger(min, max);
  return Array.from(
    { length },
    () => words[generateRandomInteger(0, words.length - 1)]
  );
}

function generateRandomSentence(words, min, max) {
  const sentence = generateRandomWords(words, min, max).join(" ");
  return sentence.charAt(0).toUpperCase() + sentence.slice(1) + ".";
}

function generateRandomParagraph(
  words,
  numSentences,
  minWordsPerSentence,
  maxWordsPerSentence
) {
  const sentences = Array.from({ length: numSentences }, () =>
    generateRandomSentence(words, minWordsPerSentence, maxWordsPerSentence)
  );
  return sentences.join(" ");
}

function generateText(
  numParagraphs,
  numSentencesPerParagraph,
  minWordsPerSentence = 5,
  maxWordsPerSentence = 15
) {
  const paragraphs = Array.from({ length: numParagraphs }, () =>
    generateRandomParagraph(
      WORDS,
      numSentencesPerParagraph,
      minWordsPerSentence,
      maxWordsPerSentence
    )
  );
  return paragraphs.join("\n");
}

module.exports = { generateText };
