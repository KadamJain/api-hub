function _200({ message = "success", data = [] }) {
  return {
    statusCode: 200,
    body: JSON.stringify({
      success: true,
      statusCode: 200,
      message,
      data,
    }),
  };
}
function _400({ message = "Bad request" }) {
  return {
    statusCode: 400,
    body: JSON.stringify({
      success: false,
      statusCode: 400,
      message,
    }),
  };
}

function _500() {
  return {
    status: 500,
    message: "Internal server error",
  };
}

module.exports = { _200, _400, _500 };
