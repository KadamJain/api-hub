function buildUrl(params) {
  let url = this,
    qs = "";
  for (let [key, value] of Object.entries(params))
    qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
  if (qs.length > 0) url = url + qs;
  return url;
}

module.exports = { buildUrl };
