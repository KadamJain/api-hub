const quoteData = require("./jsons/Quotes.json");
const connectDB = require("./config/connection");
const { v4: uuidv4 } = require("uuid");
const Quote = require("./src/models/Quotes.model");

async function script() {
  console.log(quoteData.length);
  await connectDB();
  await Promise.all(
    quoteData.map(async (quote) => {
      quote.id = uuidv4();
      await Quote.create(quote);
    })
  );
}

script()
  .then(() => console.log("success"))
  .catch((err) => console.log(err));
