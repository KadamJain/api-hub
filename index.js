const app = require("./app");
const connectDB = require("./config/connection");
require("dotenv").config();
const port = process.env.PORT || 8080;

app.listen(port, async () => {
  await connectDB();
  console.log(`App listening on port ${port}`);
});
