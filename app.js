const express = require("express");
const app = express();
const cors = require("cors");
const { errorHandler } = require("./src/middlewares/error-handler");
const planetRouter = require("./src/routes/planets.routes");
const factsRouter = require("./src/routes/facts.routes");
const textRouter = require("./src/routes/text.routes");
const qrRouter = require("./src/routes/qr.routes");
const quotesRouter = require("./src/routes/quotes.routes");
const idiomsRouter = require("./src/routes/idioms.routes");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(errorHandler);

app.use("/api/v1/planets", planetRouter);
app.use("/api/v1/facts", factsRouter);
app.use("/api/v1/generate", textRouter);
app.use("/api/v1/qr", qrRouter);
app.use("/api/v1/quotes", quotesRouter);
app.use("/api/v1/idioms", idiomsRouter);

app.get("/", (req, res) => {
  res.send("Server running fine! Start working..");
});

module.exports = app;
